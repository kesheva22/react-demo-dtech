import React,{useContext, useState} from 'react'
import profile from '../../images/profile.jpg'
import './HomPage.css'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import DeleteIcon from '@mui/icons-material/Delete';
import {useNavigate} from 'react-router-dom';
import {UserContext} from '../../context/UserDetailsContext'

function HomePage() {

    let navigate = useNavigate();

 const [userDetails, setUserDetails] = useContext(UserContext);

    //remove user from UserDetailslist
    const removeUser= (e,id) =>{
     
        let index = userDetails.findIndex(i=>i.id === id);
        let arr = [...userDetails]
   
        if(index != -1){
            arr.splice(index,1);
        }
        setUserDetails([...arr])
    }

    // back to login page
    const  navigateToLogin = (event) =>{
        navigate('/')
    }

    //to user details page for add/update details
    const addOrUpdateUserDetails = (type,id) =>{
        navigate('/userdetails',{state:{updateType:type,userId:id}})
    }

  return (
    <div >
        <div className='header-container'>

        <h1  className="home-title">HOME PAGE </h1>   <h3 className='selection' onClick={navigateToLogin} >Logout</h3>
        </div>
   <button className='add-button add-button-float' onClick={e => addOrUpdateUserDetails('add',userDetails.length)}>Add User</button>
     
        <div className="tableContainer">
    <TableContainer component={Paper} >
      <Table  aria-label="simple-table">
        <TableHead  className='tablehead'>
          <TableRow>
              
            <TableCell >Patient Id</TableCell>
            <TableCell align="right">Name</TableCell>
            <TableCell align="right">Age</TableCell>
            <TableCell align="right">Disease</TableCell>
         
            <TableCell align="right"></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {userDetails.map((item) =>  (
             <TableRow 
              key={item.name}
              className="selection" 
            >
              <TableCell component="th" scope="row" onClick={e => addOrUpdateUserDetails('update',item.id)}>
                {item.id}
              </TableCell>
   
              <TableCell align="right" onClick={e => addOrUpdateUserDetails('update',item.id)} >{item.name}</TableCell>
              <TableCell align="right" onClick={e => addOrUpdateUserDetails('update',item.id)} >{item.age}</TableCell>
              <TableCell align="right" onClick={e => addOrUpdateUserDetails('update',item.id)} >{item.disease}</TableCell>
 
              <TableCell align="right" onClick={e => removeUser(e,item.id)}> <DeleteIcon ></DeleteIcon></TableCell>
             
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
    </div>
    </div>
  )
}

export default HomePage;