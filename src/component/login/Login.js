import React,{useState} from 'react'
import './Login.css'
import {useNavigate} from 'react-router-dom'

export const Login = () => {

  let navigate = useNavigate();

    // default login id
    const demoLogin = {
        username:"admin",
        password:"password"
      }
    
      //form params
      const [userDetails, setUserDetails] = useState({username:"", password:""});
      const [error, setError] = useState("")
    
      // to home page after login and i/p error
      const submitHandler = event =>{
        event.preventDefault();
    
      
        if(userDetails.username === demoLogin.username && userDetails.password){
          setError("")
          console.log("Logged in ")
          navigate('/homepage')
        }else{
          setError("We couldn't find an account matching the login info you entered.")
          if(userDetails.password ===""){
            setError("Please enter the password")
          }
          if(userDetails.username ===""){
            setError("Please enter the username")
          }
      
        }
        
      }


  return (
    <div className="App">
           <h1 >DEMO APPLICATION</h1>
      <div className='container'>

      <form onSubmit={submitHandler}>
        <div className='form-inner'>
            <div className='form-group'>
              <input type="text" id = "username" placeholder='Username' onChange={e => setUserDetails({...userDetails, username:e.target.value})} value={userDetails.name}></input>
            </div>
            <div className='form-group'>
              <input type="password"  id = "password" placeholder='Password' onChange={e => setUserDetails({...userDetails,password:e.target.value})} value ={userDetails.password}></input>
            </div>
            {(error !== "")?(<div className="error">{error}</div>):<div className='error'></div>}
            <button type="submit" className='selection' value="Login">Login</button>
        </div>
      </form>
      </div>
    
    </div>
  )
}
