import React,{useContext,useEffect, useState} from 'react'
import './UserDetails.css'
import {useLocation} from 'react-router-dom';
import profilePic from '../../images/profile.jpg';
import {useNavigate} from 'react-router-dom';
import {UserContext} from '../../context/UserDetailsContext'

function UserDetails() {
    const location = useLocation();
    let navigate = useNavigate();
    const [userDetails, setUserDetails] = useContext(UserContext);


    const [details, setDetails ] = useState({
        id: 0,
        name: "",
        surname: "",
        age: "",
        disease: "",
        contactInfo: "",
        birthday:"",
        address: "",
        profilePicture: ""
    });

useEffect(() => {
    if(location.state.updateType ==='update'){ 
    let index = userDetails.findIndex(i=>i.id == location.state.userId);
     setDetails(userDetails[index])
     console.log(details)
    }
  },[]);


   const backToHomePage = (event) =>{
    event.preventDefault();
        navigate('/homepage')
   }


   const submitHandler = event =>{
    event.preventDefault();
    console.log('on subbimt')
    if(location.state.updateType ==='add'){ 
        details.id = userDetails.length+1;
        setUserDetails(prev=>[...prev,details])
    }else{
        let arr = [...userDetails]
        let index = userDetails.findIndex(i=>i.id == location.state.userId);
        if(index != -1){
            arr.splice(index,1);
        }
        setUserDetails([...arr,details])
    }
    navigate('/homepage')

  }

  return (
    <div>
        <h1>User Details</h1>
        <div className='user-details-container'>
        <img className ='profile-picture' src={profilePic} />
        <form onSubmit={submitHandler}>
        <div className='form-inner left-space grid-container'>
            <div className='grid-item'>
            <label className='form-label'>Name:</label>
            <label className='form-label'>Surname:</label>
            <label className='form-label'>Age:</label>
            <label className='form-label'>Contact Info:</label>
            <label className='form-label'>Disease:</label>
            <label className='form-label'>Address:</label>
            <label className='form-label'>DOB:</label>

            </div>
            <div className='grid-item'>
            <div className='form-group'>
              <input type="text" id = "Name" placeholder='Name' onChange={e => setDetails({...details, name:e.target.value})} value={details.name}></input>
            </div>
            <div className='form-group'>
              <input type="text"  id = "Surname" placeholder='Surname' onChange={e => setDetails({...details, surname:e.target.value})} value={details.surname}></input>
            </div>
            <div className='form-group'>
              <input type="text"  id = "Age" placeholder='Age' onChange={e => setDetails({...details, age:e.target.value})} value={details.age}></input>
            </div>
            <div className='form-group'>
              <input type="text"  id = "Contact Information" placeholder='Contact Information' onChange={e => setDetails({...details, contactInfo:e.target.value})} value={details.contactInfo}></input>
            </div>
            <div className='form-group'>
              <input type="text"  id = "Disease" placeholder='Disease' onChange={e => setDetails({...details, disease:e.target.value})} value={details.disease} ></input>
            </div>
            <div className='form-group'>
              <input type="text"  id = "Address" placeholder='Address' onChange={e => setDetails({...details, address:e.target.value})} value={details.address} ></input>
            </div>
            <div className='form-group'>
              <input type="text"  id = "DOB" placeholder='DOB' onChange={e => setDetails({...details, birthday:e.target.value})} value={details.birthday}></input>
            </div>
            </div>
        </div>
        <div className='submit-cancel-align'>
         <button type="submit" className='submit-okay selection' value="Login">OK</button>       
          <button  className='submit-okay selection' value="Login" onClick={e => backToHomePage(e)}>Cancel</button>
          </div>c
      </form>
        </div>
    </div>
  )
}

export default UserDetails