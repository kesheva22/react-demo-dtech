import React,{ createContext,useState } from "react";

export const UserContext = createContext();

export const UserDetialContext = props =>{

    const [userDetails, setUserDetails] = useState([{
        id: 1,
        name: "user1",
        surname: "user1",
        age: "22",
        disease: "cancer",
        contactInfo: "1234",
        birthday: "2000/11/20",
        address: "abc",
        profilePicture: ""
    },
    {
        id: 2,
        name: "user2",
        surname: "user2",
        age: "23",
        disease: "diabetes",
        contactInfo: "34444",
        birthday: "1999/11/20",
        address: "abc",
        profilePicture: ""
    }
])
    return (
        <UserContext.Provider value ={[userDetails,setUserDetails]}>
            {props.children}
        </UserContext.Provider>
    );
}