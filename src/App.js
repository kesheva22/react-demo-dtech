import './App.css';
import React,{useState} from 'react';
import { Login } from './component/login/Login';
import UserDetails from './component/userDetails/UserDetails';
import HomePage from './component/homePage/HomePage'
import { Route, BrowserRouter,Routes  } from 'react-router-dom';
import { UserDetialContext } from './context/UserDetailsContext';


function App() {

  return (
    <div>
    <BrowserRouter>
    <UserDetialContext>
    <Routes>
   
      <Route path="*" element={<Login />}></Route>
      <Route path="/homepage" element={<HomePage />}></Route>
      <Route path="/userdetails" element={<UserDetails />}></Route>
     
    </Routes>
    </UserDetialContext>
    </BrowserRouter>
    </div>
  );
}

export default App; 
